# タスク管理ツール

## 概要
```
勉強用にVue.jsを用いてタスク管理を行うためのSPAを作成しました。
ログイン画面/一覧画面/登録画面/編集画面があります。
Firebaseを用いて認証とデータベースアクセスを行っています。
```

### 使用しているライブラリ ###
```
Vuex
Vue Router
Vuetify
Vuelidate
```

### 参考にさせていただいたサイト
```
https://www.udemy.com/course/vuejs-firebase/
本格的にVue.jsの勉強をしたのは初めてだったので
かなり参考にさせていただいてます。
```