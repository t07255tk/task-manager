import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/Login.vue'
import Tasks from '@/views/Tasks.vue'
import Form from '@/views/Form.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Tasks',
    component: Tasks
  },
  {
    path: '/form',
    name: 'Add',
    component: Form
  },
  {
    path: '/form/:id',
    name: 'Edit',
    component: Form
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
