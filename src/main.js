import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import firebase from 'firebase'

Vue.config.productionTip = false
const firebaseConfig = {
  apiKey: "AIzaSyDcENzOnjkj7x_HOcaB-MXQ0uNv-vrBJ5s",
  authDomain: "taskmanager-43b7d.firebaseapp.com",
  databaseURL: "https://taskmanager-43b7d.firebaseio.com",
  projectId: "taskmanager-43b7d",
  storageBucket: "taskmanager-43b7d.appspot.com",
  messagingSenderId: "597009136990",
  appId: "1:597009136990:web:f6705de407876a16ace3cf",
  measurementId: "G-1C4D098SB8"
};

firebase.initializeApp(firebaseConfig);
firebase.analytics();

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
