import Vue from 'vue'
import Vuex from 'vuex'
import firebase from 'firebase'
import Vuelidate from 'vuelidate'

Vue.use(Vuex)
Vue.use(Vuelidate)

export default new Vuex.Store({
  state: {
    user: null,
    isOpen: false,
    tasks: []
  },
  mutations: {
    setUser(state, user) {
      state.user = user
    },
    releaseUser(state) {
      state.user = null
    },
    toggleSideMenu(state) {
      state.isOpen = !state.isOpen
    },
    initTasks(state) {
      state.tasks = []
    },
    addTask(state, { id, task }) {
      task['id'] = id
      state.tasks.push(task)
    },
    updateTask(state, { id, task }) {
      const index = state.tasks.findIndex(task => task.id === id)
      state.tasks[index] = task
    },
    deleteTask(state, id) {
      const index = state.tasks.findIndex(task => task.id === id)
      state.tasks.splice(index, 1)
    }
  },
  actions: {
    googleLogin() {
      const provider = new firebase.auth.GoogleAuthProvider()
      firebase.auth().signInWithRedirect(provider)
    },
    fetchTasks({ getters, commit }){
      commit('initTasks')
      firebase.firestore().collection('users/' + getters.userId + '/tasks').get().then(snapshots => {
        snapshots.forEach(snapshot => {
          commit('addTask', { id: snapshot.id, task: snapshot.data() })
        })
      })
    },
    logout() {
      firebase.auth().signOut()
    },
    setUser({ commit }, user) {
      commit('setUser', user)
    },
    releaseUser({ commit }) {
      commit('releaseUser')
    },
    toggleSideMenu({ commit }, e) {
      commit('toggleSideMenu', e.target.value)
    },
    addTask({ getters }, task) {
      if (getters.userId) {
        firebase.firestore().collection('users/' + getters.userId + '/tasks').add(task)
      }
    },
    updateTask({ getters, commit }, {id, task}) {
      if (getters.userId) {
        firebase.firestore().collection('users/' + getters.userId + '/tasks').doc(id).update(task).then(() => {
          commit('updateTask', { id, task })
        })
      }
    },
    deleteTask({ getters, commit }, id) {
      firebase.firestore().collection('users/' + getters.userId + '/tasks').doc(id).delete().then(() => {
        commit('deleteTask', id)
      })
    },
  },
  getters: {
    userName: state => state.user ? state.user.displayName : '',
    photoURL: state => state.user ? state.user.photoURL : '',
    email:    state => state.user ? state.user.email : '',
    userId:   state => state.user ? state.user.uid : null,
    fetchTaskById: state => id => state.tasks.filter(task => task.id === id)[0]
  },
  modules: {
  }
})
